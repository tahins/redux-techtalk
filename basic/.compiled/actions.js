"use strict";

function addTweet(text, category) {
    return {
        "type": "ADD_TWEET",
        text: text,
        category: category
    };
}

function deleteTweet(id) {
    return {
        "type": "DELETE_TWEET",
        id: id
    };
}

function favoriteTweet(id) {
    return {
        "type": "FAVORITE_TWEET",
        id: id
    };
}

module.exports = { addTweet: addTweet, deleteTweet: deleteTweet, favoriteTweet: favoriteTweet };
//# sourceMappingURL=actions.js.map