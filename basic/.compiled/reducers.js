"use strict";

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var initialState = {
    "visibilityFilter": "SHOW_ALL",
    "tweets": [{
        "id": 0,
        "text": "Hello World!!!",
        "category": "text",
        "isFavorite": false
    }, {
        "id": 1,
        "text": "This is tweet with photo,",
        "category": "image",
        "isFavorite": false
    }, {
        "id": 2,
        "text": "This is tweet with text.",
        "category": "text",
        "isFavorite": false
    }]
};

function twitterApp() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
        case "ADD_TWEET":
            return _extends(state, {
                tweets: addTweet(state.tweets, action)
            });

        case "DELETE_TWEET":
            return _extends(state, {
                tweets: deleteTweet(state.tweets, action)
            });

        case "FAVORITE_TWEET":
            return _extends(state, {
                tweets: favoriteTweet(state.tweets, action)
            });

        default:
            return state;
    }
}

function addTweet() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments[1];

    var tweets = state.slice();
    tweets.push({
        "id": state.length,
        "text": action.text,
        "category": action.category,
        "isFavorite": false
    });

    return tweets;
}

function deleteTweet() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments[1];

    return state.filter(function (item, index) {
        return item.id !== action.id;
    });
}

function favoriteTweet(state, action) {
    return state.map(function (item, index) {
        return item.id === action.id ? _extends(item, { "isFavorite": !item.isFavorite }) : item;
    });
}

module.exports = twitterApp;
//# sourceMappingURL=reducers.js.map