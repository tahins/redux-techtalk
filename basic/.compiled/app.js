'use strict';

var _store = require('./store');

var _store2 = _interopRequireDefault(_store);

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log("------ Initial State ------");
console.log(_store2.default.getState());
console.log("---------------------------");

_store2.default.dispatch(_actions2.default.addTweet("This is another Tweet", "text"));

console.log("----- After New Tweet -----");
console.log(_store2.default.getState());
console.log("---------------------------");

_store2.default.dispatch(_actions2.default.deleteTweet(2));

console.log("---- After Delete Tweet ---");
console.log(_store2.default.getState());
console.log("---------------------------");

_store2.default.dispatch(_actions2.default.favoriteTweet(1));

console.log("---- After Favorite Tweet ---");
console.log(_store2.default.getState());
console.log("-----------------------------");
//# sourceMappingURL=app.js.map