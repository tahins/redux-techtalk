import twitterApp from './reducers';

var masterState = twitterApp();

function getState () {
    return masterState;
};

function dispatch (action) {
    masterState = twitterApp(masterState, action);
    return masterState;
};

module.exports = {
    getState, dispatch
}