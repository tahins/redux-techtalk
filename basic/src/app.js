import store from './store'
import actions from './actions'

console.log("------ Initial State ------")
console.log(store.getState())
console.log("---------------------------")

store.dispatch(actions.addTweet("This is another Tweet", "text"))

console.log("----- After New Tweet -----")
console.log(store.getState())
console.log("---------------------------")

store.dispatch(actions.deleteTweet(2))

console.log("---- After Delete Tweet ---")
console.log(store.getState())
console.log("---------------------------")

store.dispatch(actions.favoriteTweet(1))

console.log("---- After Favorite Tweet ---")
console.log(store.getState())
console.log("-----------------------------")