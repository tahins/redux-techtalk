function addTweet(text, category) {
    return {
        "type": "ADD_TWEET",
        text,
        category
    };
}

function deleteTweet(id) {
    return {
        "type": "DELETE_TWEET",
        id
    };
}

function favoriteTweet(id) {
    return {
        "type": "FAVORITE_TWEET",
        id
    };
}

module.exports = { addTweet, deleteTweet, favoriteTweet }