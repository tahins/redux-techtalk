const initialState =  {
    "visibilityFilter": "SHOW_ALL",
    "tweets": [
        {
            "id": 0,
            "text": "Hello World!!!",
            "category": "text",
            "isFavorite": false
        },
        {
            "id": 1,
            "text": "This is tweet with photo,",
            "category": "image",
            "isFavorite": false
        },
        {
            "id": 2,
            "text": "This is tweet with text.",
            "category": "text",
            "isFavorite": false
        }
    ]
};

function twitterApp(state = initialState, action = {}) {
    switch (action.type) {
        case "ADD_TWEET":
            return Object.assign(state, {
                tweets: addTweet(state.tweets, action)
            });

        case "DELETE_TWEET":
            return Object.assign(state, {
                tweets: deleteTweet(state.tweets, action)
            });

        case "FAVORITE_TWEET":
            return Object.assign(state, {
                tweets: favoriteTweet(state.tweets, action)
            });
    
        default:
            return state;
    }
}

function addTweet(state = [], action) {
    var tweets = state.slice();
    tweets.push({
        "id": state.length,
        "text": action.text,
        "category": action.category,
        "isFavorite": false
    });

    return tweets;
}

function deleteTweet(state = [], action) {
    return state.filter((item, index) => item.id !== action.id)
}

function favoriteTweet(state, action) {
    return state.map((item, index) => (item.id === action.id)? 
        Object.assign(item, {"isFavorite": !item.isFavorite}) : item)
}

module.exports = twitterApp;