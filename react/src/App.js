import React, { Component } from 'react';
import './App.css';
import AddTweet from './components/addTweet/AddTweet';
import TweetFilter from './components/tweetFilter/TweetFilter';
import TweetList from './components/tweetList/TweetList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AddTweet/>
        <TweetFilter/>
        <TweetList/>
      </div>
    );
  }
}

export default App;
