import React from 'react'
import { connect } from 'react-redux'
import { addTweet } from '../../actions/index'

let AddTweet = ({ dispatch }) => {
  let input, category

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault()
          if (!input.value.trim()) {
            return
          }
          dispatch(addTweet(input.value, category.value))
          input.value = ''
          category.value = 'text'
        }}
      >
        <input
          ref={node => {
            input = node
          }}
        />
        <select ref={node => {
            category = node
          }}>
            <option value="text">Text</option>
            <option value="image">Image</option>
        </select>
        <button type="submit">
          Tweet
        </button>
      </form>
    </div>
  )
}
AddTweet = connect()(AddTweet)

export default AddTweet