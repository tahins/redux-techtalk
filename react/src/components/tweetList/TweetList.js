import { connect } from 'react-redux'
import TweetListView from './TweetListView'
import { removeTweet, favoriteTweet } from '../../actions/index';

const getVisibleTweets = (tweets, filter) => {
  switch (filter) {
    case 'SHOW_TEXT':
      return tweets.filter(t => t.category === "text")
    case 'SHOW_IMAGE':
      return tweets.filter(t => t.category === "image")
    case 'SHOW_ALL':
    default:
      return tweets
  }
}

const mapStateToProps = state => {
  return {
    tweets: getVisibleTweets(state.tweets, state.visibilityFilter)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTweetRemove: id => {
      dispatch(removeTweet(id))
    },
    onTweetFavorite: id => {
      dispatch(favoriteTweet(id))
    }
  }
}

const TweetList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TweetListView)

export default TweetList