import React from 'react'

const Tweet = ({ id, text, isFavorite, removeTweet, favoriteTweet }) => (
  <li>
    {text}
    <br/>
    <a href="javascript:void(0)" onClick={favoriteTweet}>{isFavorite? "unfavorite" : "favorite"}</a>
    &nbsp;-&nbsp;
    <a href="javascript:void(0)" onClick={removeTweet}>remove</a>
  </li>
)

export default Tweet