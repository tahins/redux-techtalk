import React from 'react'
import Tweet from './Tweet'

const TweetListView = ({ tweets, onTweetRemove, onTweetFavorite }) => (
  <ul>
    {tweets.map(tweet => (
      <Tweet key={tweet.id} {...tweet} 
        removeTweet={() => onTweetRemove(tweet.id)} 
        favoriteTweet={() => onTweetFavorite(tweet.id)}/>
    ))}
  </ul>
)

export default TweetListView