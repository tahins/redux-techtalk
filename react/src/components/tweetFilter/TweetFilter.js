import { connect } from 'react-redux'
import { setVisibilityFilter } from '../../actions/index'
import TweetFilterView from './TweetFilterView'

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.visibilityFilter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: (filter) => {
      dispatch(setVisibilityFilter(filter))
    }
  }
}

const TweetFilter = connect(
  mapStateToProps,
  mapDispatchToProps
)(TweetFilterView)

export default TweetFilter