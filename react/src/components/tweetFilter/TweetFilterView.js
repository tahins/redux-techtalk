import React from 'react'

const TweetFilterView = ({ onClick }) => {
  return (
    <p><br/>
    Show:
    {' '}
    <a href="javascript:void(0)" onClick={e => {
        e.preventDefault()
        onClick("SHOW_ALL")
      }}
    >
      All
    </a>
    {', '}
    <a href="javascript:void(0)" onClick={e => {
        e.preventDefault()
        onClick("SHOW_TEXT")
      }}
    >
      Text
    </a>
    {', '}
    <a href="javascript:void(0)" onClick={e => {
        e.preventDefault()
        onClick("SHOW_IMAGE")
      }}
    >
      Image
    </a>
  </p>
    
  )
}

export default TweetFilterView