const tweets = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TWEET':
            return [
                ...state,
                {
                    id: action.id,
                    text: action.text,
                    isFavorite: false,
                    category: action.category
                }
            ]

        case 'REMOVE_TWEET':
            return state.filter(item => item.id !== action.id)

        case 'FAVORITE_TWEET':
            return state.filter(item => (item.id === action.id)? Object.assign(item, {"isFavorite": !item.isFavorite}) : item)
    
        default:
            return state
    }
  }
  
  export default tweets