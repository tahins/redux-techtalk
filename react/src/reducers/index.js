import { combineReducers } from 'redux'
import tweets from './tweets'
import visibilityFilter from './visibilityFilter'

const twitterApp = combineReducers({
    tweets,
    visibilityFilter
})

export default twitterApp