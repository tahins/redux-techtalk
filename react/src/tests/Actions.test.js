import * as actions from '../actions/index'

describe('ADD_TWEET action', () => {
    it('should create an action for adding a new tweet', () => {
        const text = 'hello world!'
        const category = 'text'
        const expectedAction = {
            "type": "ADD_TWEET",
            "id": 0,
            text,
            category
        }

        expect(actions.addTweet(text, category)).toEqual(expectedAction)
    });
});