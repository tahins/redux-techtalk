import tweets from '../reducers/tweets'

describe('Tweet reducer', () => {
    it('should return initial state', () => {
        const state = tweets(undefined, {})
        
        expect(state).toEqual([])
    });

    it('should add tweet', () => {
        const text = 'hello world!'
        const category = 'text'
        const action = {
            "type": "ADD_TWEET",
            "id": 0,
            text,
            category
        }
        const state = tweets([], action)
        
        expect(state).toEqual([{
            "id": 0,
            "isFavorite": false,
            text,
            category
        }])
    });

    it('should favorite a tweet', () => {
        const text = 'hello world!'
        const category = 'text'
        const action = {
            "type": "FAVORITE_TWEET",
            "id": 0
        }
        const commonState = {
            "id": 0,
            text,
            category
        }
        const state = tweets([{
            ...commonState,
            "isFavorite": false
        }], action)
        
        expect(state).toEqual([{
            ...commonState,
            "isFavorite": true
        }])
    });

    it('should remove tweet', () => {
        const text = 'hello world!'
        const category = 'text'
        const action = {
            "type": "REMOVE_TWEET",
            "id": 0
        }
        const state = tweets([{
            "id": 0,
            "isFavorite": false,
            text,
            category
        }], action)
        
        expect(state).toEqual([])
    });
});