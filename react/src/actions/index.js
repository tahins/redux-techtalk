let nextTweetId = 0
export const addTweet = (text, category) => {
  return {
    type: 'ADD_TWEET',
    id: nextTweetId++,
    text,
    category
  }
}

export const removeTweet = (id) => {
    return {
      type: 'REMOVE_TWEET',
      id
    }
  }

export const favoriteTweet = (id) => {
    return {
      type: 'FAVORITE_TWEET',
      id
    }
  }  

export const setVisibilityFilter = filter => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}